@extends('plantilla')
@section('contenido')
<!-- Contenido-->
<section class="content">
    <h1>Contáctanos</h1>
    <form style="width: 26rem;" method="POST" action="{{route('contacto')}}">
        @csrf
        <!-- Name input -->
        <div data-mdb-input-init class="form-outline mb-4">
          <input type="text" id="nombre" class="form-control" />
          <label class="form-label" for="nombre">Nombre</label>
        </div>
      
        <!-- Email input -->
        <div data-mdb-input-init class="form-outline mb-4">
          <input type="email" id="email" class="form-control" />
          <label class="form-label" for="email">Correo</label>
        </div>

        <div data-mdb-input-init class="form-outline mb-4">
            <input type="text" id="asunto" class="form-control" />
            <label class="form-label" for="asunto">Asunto</label>
          </div>
      
        <!-- Message input -->
        <div data-mdb-input-init class="form-outline mb-4">
          <textarea class="form-control" id="mensaje" rows="4"></textarea>
          <label class="form-label" for="mensaje">Mensaje</label>
        </div>
      
        <!-- Submit button -->
        <button data-mdb-ripple-init type="submit" class="btn btn-primary btn-block mb-4">Enviar</button>
      </form>
</section>
@endsection