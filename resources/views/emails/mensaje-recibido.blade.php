<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Mensaje Recibido</title>
    </head>
    <body>
        <p>Recibiste un mensaje de: {{$mailData['nombre']}} - {{$mailData['email']}}</p>
        <p><strong>Asunto: </strong>{{$mailData['asunto']}}</p>
        <p><strong>Contenido: </strong>{{$mailData['contenido']}}</p>
    </body>
</html>