<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MensajeRecibido;

class MensajesController extends Controller
{
    public function store(){
        $mailData=request()->validate([
            'nombre'=>'required',
            'email'=>'required|email',
            'asunto'=>'required',
            'mensaje'=>'required|min:3'
        ],[
            'nombre.required'=>__('El nombre es requerido'),
            'email.required'=>__('El correo es requerido'),
            'asunto.required'=>__('El asunto es requerido'),
            'mensaje.required'=>__('El mensaje es requerido')
        ]);
    Mail::to('hello@example.com')->cc($mailData['email'])->send(new MensajeRecibido($mailData));
        return redirect()->route('principal');
        //return 'Mensaje Enviado';
    }
}
